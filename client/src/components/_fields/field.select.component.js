import React, {useContext} from "react"
import {GenerateContext} from "../../context/GenerateContext"
import {Form} from "react-bootstrap"
import {PreloaderContext} from "../../context/PreloaderContext";

export const FieldSelect = ({value, onChange, indexRow}) => {
    const {getTemplate} = useContext(GenerateContext)
    const preloader = useContext(PreloaderContext)
    return (

        <Form.Select
            data-index={indexRow}
            onChange={onChange}
            defaultValue={value}
            disabled={preloader.preloader}
        >
            {getTemplate.map((option, key) => {
                return (
                    <option value={option.key}
                            key={key}>{option.label}</option>
                )
            })}
        </Form.Select>
    )
}