import React from "react"
import {BrowserRouter as Router} from 'react-router-dom'
import {useRoutes} from "./routes"
import {AuthContext} from "./context/AuthContext"
import {PreloaderContext} from "./context/PreloaderContext"
import {CookieContext} from "./context/CookieContext"
import {useAuth} from "./hooks/auth.hook"
import {usePreloader} from "./hooks/preloader.hook"
import {useCookie} from "./hooks/cookie.hook"
import {Navbar} from "./components/Navbar"
import 'bootstrap/dist/css/bootstrap.min.css'
import "@fontsource/poppins"
import './assets/general.scss'
import Cookie from "./components/Cookie";


// { isAuthenticated && <Navbar/> }
function App() {
    const {token, login, logout, userId} = useAuth()
    const isAuthenticated = !!token
    const routes = useRoutes(isAuthenticated)
    return (
        <>
            <PreloaderContext.Provider value={usePreloader()}>
                <CookieContext.Provider value={useCookie()}>
                    <AuthContext.Provider value={{
                        token, login, logout, userId, isAuthenticated
                    }}>
                        <Router>
                            { <Navbar/> }
                            {routes}
                        </Router>
                    </AuthContext.Provider>
                    <Cookie/>
                </CookieContext.Provider>
            </PreloaderContext.Provider>
        </>
    )
}

export default App
