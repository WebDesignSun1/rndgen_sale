const express = require('express')
const path = require('path')
const DB = require('./connect/db')
require('dotenv').config();

const app = express()

DB.connectDB()

app.use('/favicon.ico', express.static('favicon.ico'));

app.use(require('prerender-node').set('prerenderToken', 'TOKEN'));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json({extended: true}))

app.use('/api/auth', require('./routes/auth.routes'))
app.use('/api/generate', require('./routes/generate.routes'))

if (process.env.NODE_ENV === 'production') {
    app.use('/', express.static(path.join(__dirname, 'client', 'build')))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
}


const PORT = (process.env.NODE_ENV === 'production') ? process.env.PORT_PRODUCTION || 5000 : process.env.PORT_DEVELOPER || 5000

app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`))
