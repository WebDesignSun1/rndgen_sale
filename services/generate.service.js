const RandomHandler = require("../services/Handlers/random.service")
const CsvHandler = require("../services/Format/csv.service")
const ExcelHandler = require("../services/Format/excel.sercive")
const JsonHandler = require("../services/Format/json.service")
const SqlHandler = require("../services/Format/sql.service")
const XmlHandler = require("../services/Format/xml.service")
const PreviewHandler = require("../services/Format/preview.service")

module.exports = class GenerateService {
    constructor() {
        this.randomHandler = new RandomHandler()
        this.csvHandler = new CsvHandler()
        this.excelHandler = new ExcelHandler()
        this.jsonHandler = new JsonHandler()
        this.sqlHandler = new SqlHandler()
        this.xmlHandler = new XmlHandler()
        this.previewHandler = new PreviewHandler()
    }

    init(data) {
        const {extension, preview} = data

        let outputExport,
            outputPreview = this.previewHandler.init(data)

        switch (extension) {
            case 'json':
                outputExport = this.jsonHandler.init(data)
                break
            case 'sql':
                outputExport = this.sqlHandler.init(data)
                break
            case 'xml':
                outputExport = this.xmlHandler.init(data)
                break
            case 'csv':
                outputExport = this.csvHandler.init(data)
                break
            case 'xlsx':
                outputExport = preview
                    ? this.csvHandler.init(data, false)
                    : this.excelHandler.init(data)
                break
        }

        return {
            export: outputExport,
            preview: outputPreview
        }
    }
}