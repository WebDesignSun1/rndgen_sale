const RandomHandler = require("../../services/Handlers/random.service")

module.exports = class CsvHandler {

    constructor() {
        this.randomHandler = new RandomHandler()
        this.response = []
    }

    init(data, defparam = true){
        const {columns, count, setting, preview} = data
        let quote = `"`,
            separator = `;`,
            end_line = `\n`,
            escape = `'`

        if (setting.fields.include_header.value === 'yes') {
            this.response.push(columns.map(item => item.name))
        }

        for(let i = 0; i < count; i++){
            let line = Object.values(this.randomHandler.initLine(i, columns))
            this.response.push(line)
        }

        if(defparam) {
            quote = setting.fields.quote.value
            separator = setting.fields.delimiter.value
            end_line = setting.fields.end_line.value
            escape = setting.fields.escape.value
        }

        return this.csvGenerateRows(
            this.response,
            quote,
            separator,
            end_line,
            escape
        )
    }

    csvGenerateRows(rows, quote, separator, end_line, escape) {
        return rows
            .filter(e => e)
            .map(
                row => row
                    .map((element) => (typeof element === 'undefined' || element === null) ? '' : element)
                    .map(column => {
                        const regQuote = /'/ig
                        const reqDoubleQuote = /"/ig
                        switch (quote) {
                            case `"`:
                                column = column.toString().replace(reqDoubleQuote, `${escape}"`)
                                break
                            case `'`:
                                column = column.toString().replace(regQuote, `${escape}'`)
                                break
                        }

                        return `${quote}${column}${quote}`
                    })
                    .join(separator)
            )
            .join(end_line)
    }
}