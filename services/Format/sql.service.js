const RandomHandler = require("../../services/Handlers/random.service")

module.exports = class SqlHandler {

    constructor() {
        this.randomHandler = new RandomHandler()
        this.response = ''
    }

    init(data){
        const {columns, count, setting} = data

        if (setting.fields.include_header.value === 'yes') {
            let values = columns.map(item => {
                switch (item.key){
                    case 'alpha':
                        return `${item.name} ${item.type}(${item.fields.count.value})`
                    case 'bool':
                        return `${item.name} ${item.type}(1)`
                    case 'digit':
                        return `${item.name} ${item.type}(${item.fields.length.value})`
                    case 'word':
                        return `${item.name} ${item.type}(${item.fields.length.value})`
                }

                switch (item.type){
                    case 'varchar':
                        return `${item.name} ${item.type}(255)`
                    case 'float':
                        return `${item.name} ${item.type}(${String(item.fields.max.value).length},${item.fields.scale.value})`
                    default :
                        return `${item.name} ${item.type}`
                }
            })

            this.response += `Create Table ${setting.fields.table_name.value} ( ${values.join(', ')} ); \n`
        }

        for(let i = 0; i < count; i++){
            let line = Object.entries(this.randomHandler.initLine(i, columns))

            const headerSql = columns.map(item => item.name).join(', ')
            const tableName = setting.fields.table_name.value;
            const sqlValue = []

            line.forEach(([key, value], i) => {
                if (typeof value === 'string' || value instanceof String) {
                    sqlValue.push(`'${value.toString().replace(/'/ig, `\\'`)}'`)
                } else {
                    sqlValue.push(value)
                }
            })
            this.response += `Insert Into ${tableName} (${headerSql}) Values (${sqlValue.join(', ')});  \n`
        }

        return this.response
    }
}