const RandomHandler = require("../../services/Handlers/random.service")

module.exports = class PreviewHandler {

    constructor() {
        this.randomHandler = new RandomHandler()
        this.response = []
    }

    init(data){
        const {columns, count, preview} = data,
            header = columns.map(item => item.name)

        if(!preview){
            return this.response
        }

        this.response.push(header)

        for(let i = 0; i < count; i++){
            let line = this.randomHandler.initLine(i, columns)
            this.response.push(line)
        }

        return this.response
    }
}