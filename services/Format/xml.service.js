const RandomHandler = require("../../services/Handlers/random.service")

module.exports = class XmlHandler {

    constructor() {
        this.randomHandler = new RandomHandler()
        this.response = ''
    }

    init(data){
        const {columns, count, setting} = data,
            tagRoot = setting.fields.root_tag.value

        for(let i = 0; i < count; i++){
            let line = Object.entries(this.randomHandler.initLine(i, columns)),
                records = [],
                tagRecord = setting.fields.record_tag.value;

            line.forEach(([key,value], i) => {
                records.push(`\t\t\t<${key}>${value}</${key}>\n`)
            })
            this.response += `\t\t<${tagRecord}>\n${records.join('')}\t\t</${tagRecord}>\n`
        }

        return `<${tagRoot}>\n${this.response}\n</${tagRoot}>`
    }
}