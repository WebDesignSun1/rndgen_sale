const RandomHandler = require("../../services/Handlers/random.service")

module.exports = class JsonHandler {

    constructor() {
        this.randomHandler = new RandomHandler()
        this.response = []
    }

    init(data){
        const {columns, count, setting} = data

        let values = setting.fields.type.value === 'list';

        for(let i = 0; i < count; i++){
            let line = this.randomHandler.initLine(i, columns, values)
            this.response.push(line)
        }

        return JSON.stringify(this.response, undefined, 4)
    }
}