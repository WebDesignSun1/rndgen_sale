const RandomHandler = require("../../services/Handlers/random.service")

module.exports = class ExcelHandler {

    constructor() {
        this.randomHandler = new RandomHandler()
        this.response = []
    }

    init(data){
        const {columns, count, setting} = data

        if (setting.fields.include_header.value === 'yes') {
            let header = columns.map(item => {
                return {
                    value: item.name
                }
            })
            this.response.push(header)
        }

        for(let i = 0; i < count; i++){
            let line = Object.values(this.randomHandler.initLine(i, columns)),
                excelData = []

            line.forEach(item => excelData.push({
                value: item
            }))

            this.response.push(excelData)
        }

        return this.response
    }
}